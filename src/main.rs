use std::env;
use std::fs::OpenOptions;
use std::time;

use chrono::{prelude::*, Weekday};
use log::{error, info, LevelFilter};
use rand::{self, seq::SliceRandom, thread_rng, Rng};
use reqwest::header::AUTHORIZATION;
use serde::Serialize;
use simplelog::{
    ColorChoice, CombinedLogger, ConfigBuilder, TermLogger, TerminalMode, WriteLogger,
};
use sqlx::{postgres::PgPoolOptions, Pool, Postgres};

/// The status that will get posted.
#[derive(Serialize, Debug)]
struct StatusToPost {
    status: String,
}

#[derive(Debug)]
enum StatusKind {
    AdvocacyTip,
    FediAccountRec,
    OrganizationRec,
}

impl StatusKind {
    /// Determine if a `StatusKind` is configured, in the .env file, to be posted.
    fn configured_to_post(&self) -> bool {
        let key = match self {
            StatusKind::AdvocacyTip => "POST_ADVOCACY_TIP",
            StatusKind::FediAccountRec => "POST_FEDI_ACCOUNT_REC",
            StatusKind::OrganizationRec => "POST_ORGANIZATION_REC",
        };
        match env::var(key) {
            Ok(v) => match v.trim().parse() {
                Ok(v) => v,
                Err(e) => {
                    error!("Unable to parse {key} as boolean: {e}.");
                    false
                }
            },
            Err(e) => {
                error!("Unable to load {key} from .env file: {e}.");
                false
            }
        }
    }
}

#[derive(sqlx::FromRow, Debug)]
struct AdvocacyTip {
    text: String,
    source: Option<String>,
}

#[derive(sqlx::FromRow, Debug)]
struct Account {
    account: String,
    kind: String,
}

#[derive(sqlx::FromRow, Debug)]
struct Organization {
    name: String,
    url: String,
}

#[tokio::main]
async fn main() {
    dotenvy::dotenv().expect("Unable to load .env file");
    // Set up logging, panic if it fails.
    let config = ConfigBuilder::new().set_time_format_rfc3339().build();
    CombinedLogger::init(vec![
        TermLogger::new(
            LevelFilter::Debug,
            config.clone(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        WriteLogger::new(
            LevelFilter::Info,
            config,
            OpenOptions::new()
                .append(true)
                .create(true)
                .open("bot.log")
                .expect("Could not open log file."),
        ),
    ])
    .expect("Could not configure logging.");

    info!("Starting bot.");

    let authorization = match env::var("ACCESS_TOKEN") {
        Ok(v) => format!("Bearer {v}"),
        Err(e) => {
            return error!("Unable to load ACCESS_TOKEN from .env file: {e}.");
        }
    };

    let base_url = match env::var("BASE_URL") {
        Ok(v) => v,
        Err(e) => {
            return error!("Unable to load BASE_URL from .env file: {e}.");
        }
    };

    let database = match env::var("DATABASE_URL") {
        Ok(v) => v,
        Err(e) => {
            return error!("Unable to load DATABASE_URL from .env file: {e}.");
        }
    };

    let pool = match PgPoolOptions::new()
        .max_connections(5)
        .connect(&database)
        .await
    {
        Ok(v) => v,
        Err(e) => return error!("Unable to create database connection pool: {e}."),
    };

    let client = reqwest::Client::new();
    let status_endpoint = format!("{base_url}/api/v1/statuses");

    // time between loops
    let sleep_time = time::Duration::from_secs(60 * 60);

    loop {
        // Set bot's status based on day of the week/hour.
        // If in production, create various kinds depending on day of week/hour of day.
        let status = if env::var("PRODUCTION").is_ok() {
            let now = chrono::Utc::now();
            let hour = now.hour();
            match now.weekday() {
                Weekday::Mon => match hour {
                    21 => create_status(&pool, &StatusKind::AdvocacyTip).await,
                    _ => None,
                },
                Weekday::Tue => None,
                Weekday::Wed => match hour {
                    17 => create_status(&pool, &StatusKind::OrganizationRec).await,
                    _ => None,
                },
                Weekday::Thu => None,
                Weekday::Fri => match hour {
                    13 => create_status(&pool, &StatusKind::FediAccountRec).await,
                    _ => None,
                },
                Weekday::Sat => None,
                Weekday::Sun => None,
            }
        } else {
            // In development, change this to whatever kind of status I'm trying to create.
            create_status(&pool, &StatusKind::FediAccountRec).await
        };

        // Either post to server or print to stdout based on environment.
        if env::var("PRODUCTION").is_ok() {
            if let Some(mut v) = status {
                // Add vegan tag to it first
                v.status += "\n#vegan";
                match client
                    .post(&status_endpoint)
                    .header(AUTHORIZATION, &authorization)
                    .form(&v)
                    .send()
                    .await
                {
                    Ok(_) => (),
                    Err(e) => error!("Error posting status: {e}"),
                };
            }
        } else {
            dbg!(status); // just display status if not in production
            return;
        }
        tokio::time::sleep(sleep_time).await;
    }
}

/// Create a status to post.
async fn create_status(pool: &Pool<Postgres>, kind: &StatusKind) -> Option<StatusToPost> {
    // return None if `kind` is not configured to be posted
    if !kind.configured_to_post() {
        info!("{kind:?} is not configured to post; ignoring attempt.");
        return None;
    }
    let status = match kind {
        StatusKind::AdvocacyTip => {
            // Get all AdvocacyTips.
            let tips = match sqlx::query_as!(AdvocacyTip, "SELECT text, source from advocacy_tips")
                .fetch_all(pool)
                .await
            {
                Ok(v) => v,
                Err(e) => {
                    error!("Unable to create status to post: {e}");
                    return None;
                }
            };
            if tips.is_empty() {
                return None;
            }
            let tip = get_random_item(tips);
            let mut status = format!("Advocacy tip: {}", tip.text);
            if let Some(ref v) = tip.source {
                status.push_str(&format!("\n\n{v}"));
            }
            status
        }
        StatusKind::FediAccountRec => {
            // Get all follow recommendations
            let mut recs = match sqlx::query_as!(Account, r#"select account, kind from accounts where last_used < CURRENT_DATE - INTERVAL '90 day' OR last_used IS NULL"#)
                .fetch_all(pool)
                .await
            {
                Ok(v) => v,
                Err(e) => {
                    error!("Unable to create status to post: {e}");
                    return None;
                }
            };

            if recs.is_empty() {
                return None;
            }

            let mut status =
                "Hello, humans. Here are some #FollowFriday recommendations. These are predominantly in English and without a specific focus (beyond vegan), but when that's not the case, it'll be noted in parentheses:\n".to_string();
            recs.shuffle(&mut thread_rng());
            recs.truncate(3);
            for account in recs {
                // Update last_used field in db table.
                match sqlx::query_as!(
                    Account,
                    "update accounts set last_used = CURRENT_DATE where account = $1",
                    account.account
                )
                .execute(pool)
                .await
                {
                    Ok(_) => (),
                    Err(e) => {
                        error!("Unable to update last_used field: {e}");
                    }
                };

                if account.kind == "general" {
                    status.push_str(&format!("\n{}", account.account));
                } else {
                    status.push_str(&format!("\n{} ({})", account.account, account.kind));
                }
            }

            status.push_str(
                "\n\n#ff\n\nIf you have any recommendations for me, please feel free to reply!",
            );
            status
        }
        StatusKind::OrganizationRec => {
            let mut orgs =
                match sqlx::query_as!(Organization, "select name, url from organizations",)
                    .fetch_all(pool)
                    .await
                {
                    Ok(v) => v,
                    Err(e) => {
                        error!("Unable to create status to post: {e}");
                        return None;
                    }
                };
            if orgs.is_empty() {
                return None;
            }
            let mut status = "Organizations worth checking out:\n".to_string();
            orgs.shuffle(&mut thread_rng());
            orgs.truncate(3);
            for org in orgs {
                status.push_str(&format!("\n{}: {}", org.name, org.url));
            }

            status
        }
    };
    Some(StatusToPost { status })
}

fn get_random_item<T>(mut items: Vec<T>) -> T {
    let upper_bound = items.len();
    let mut rng = rand::thread_rng();
    let random_int = rng.gen_range(0..upper_bound);
    items.remove(random_int)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn create_status_fedi_account_rec_success() {
        dotenvy::dotenv().expect("Unable to load .env file");
        let pool = PgPoolOptions::new()
            .max_connections(1)
            .connect(&env::var("DATABASE_URL").unwrap())
            .await
            .unwrap();
        let kind = StatusKind::FediAccountRec;

        let status = create_status(&pool, &kind).await;
        if kind.configured_to_post() {
            assert!(status.is_some());
            assert!(status.unwrap().status.contains("#FollowFriday"));
        } else {
            assert!(status.is_none())
        }
    }

    #[tokio::test]
    async fn create_status_organization_rec() {
        dotenvy::dotenv().expect("Unable to load .env file");
        let pool = PgPoolOptions::new()
            .max_connections(1)
            .connect(&env::var("DATABASE_URL").unwrap())
            .await
            .unwrap();
        let kind = StatusKind::OrganizationRec;
        let status = create_status(&pool, &kind).await;
        if kind.configured_to_post() {
            assert!(status.is_some());
            assert!(status.unwrap().status.contains("Organization"));
        } else {
            assert!(status.is_none())
        }
    }
}
