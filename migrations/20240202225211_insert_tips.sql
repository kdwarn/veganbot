-- Add migration script here
INSERT INTO advocacy_tips (text) VALUES ('Find a friend and distribute "Why Vegan?" pamphlets at a local college campus. Go to https://veganoutreach.org/volunteer-communityoutreach/ to learn more.');

INSERT INTO advocacy_tips (text) VALUES ('Volunteer at a farm sanctuary. If you''re able to do it regularly, perhaps you can interest a non-vegan friend to join you. Check out https://sanctuaryfederation.org/find-a-sanctuary/.');

INSERT INTO advocacy_tips (text) VALUES ('Find a mentor. Whether you''re brand-new or not-so-new to being vegan or an animal rights advocate, a mentor may help you navigate the terrain. Or maybe you could mentor someone else? \n\n
    https://veganoutreach.org/vegan-mentorship-program/ \n\n
    http://animalrightscoalition.com/programs/veganuniversity/ \n\n
    https://3movies.org/aramovement');

INSERT INTO advocacy_tips (text) VALUES ('In Defense of Animals'' Sustainable Activism campaign (https://www.idausa.org/campaign/sustainable-activism/) "support[s] animal activists by providing emotional and spiritual resources including a support line, a monthly online support group, activist resource list, sustainable activism webinars, activist healing retreats, vegan spirituality online gatherings, religion-specific vegan advocacy kits, and other faith-based resources from our Interfaith Vegan Coalition." The many sustainable activism webinars are available on YouTube: <https://www.youtube.com/playlist?list=PLoF5qlifvW6GKn3t-d_ax1O4vGoXSkDbz>.');

INSERT INTO advocacy_tips (text) VALUES ('If there''s a vegan activist group in your community, join it! Meet other vegans and help spread the word. Or, in the likely event that there isn''t one already, start one! Read this excellent article to help you get it going: https://vegan.com/info/local-vegan-activism/.');

INSERT INTO advocacy_tips (text) VALUES ('Host a film screening. Dominion? Cowspiracy? Something else? VegFund has a guide on putting one together and a list of recommended films: https://vegfund.org/resource/the-definitive-guide-to-hosting-a-vegan-film-screening');
-- INSERT INTO advocacy_tips (text) VALUES ('');
