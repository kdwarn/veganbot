-- Add migration script here
alter table accounts drop constraint accounts_kind_check;
alter table accounts add constraint accounts_kind_check CHECK (kind in (
    'general',
    'cooking',
    'fitness',
    'organization',
    'app/tool',
    'news',
    'French',
    'German',
    'Dutch',
    'Spanish'
));
