-- Add migration script here
CREATE TABLE IF NOT EXISTS advocacy_tips (
    id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    text text NOT NULL,
    source text
);
