-- Add migration script here
CREATE TABLE organizations (
    id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name text UNIQUE NOT NULL,
    url text NOT NULL CHECK(starts_with(url, 'http'))
);
