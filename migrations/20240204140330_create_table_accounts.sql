-- Add migration script here
CREATE TABLE accounts (
    id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    account text NOT NULL CHECK (starts_with(account, '@')),
    kind text NOT NULL,
    last_used date,
    CHECK (kind in (
        'general',
        'cooking',
        'fitness',
        'organization',
        'app/tool',
        'news',
        'French',
        'German',
        'Dutch'
    )),
    UNIQUE (account, kind)
);


